import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

//services
import {Feed, HttpService} from '../../core/services/http.service';

@Component({
    selector: 'album-table',
    templateUrl: './album-table.component.html',
    styleUrls: ['./album-table.component.scss']
})
export class AlbumTableComponent implements OnInit{

    filterValue: string;
    filterType: string = 'album';
    itemsPerPage: number = 10;
    currentPage: number = 1;
    feed: Feed;

    constructor(
        private router: Router,
        private httpService: HttpService
    ){

    }

    ngOnInit(){
        this.httpService.getFeed().subscribe((data: Feed) => this.feed = { ...data });
    }

    isRowOdd(i: number): boolean{
        return i % 2 === 0;
    }

    getItemIndex(index: number, page: number): number{
        return (index + 1) + ((page - 1) * this.itemsPerPage);
    }

    viewSong(song: any){
        let id: number;
        if(song && song.id && song.id.attributes && song.id.attributes){
            id = song.id.attributes['im:id'];
            if(id){
                this.router.navigate(['song', id]);
            }
        }
    }

}