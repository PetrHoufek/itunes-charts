import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NgxPaginationModule} from 'ngx-pagination';

//components
import {DashboardComponent} from './dashboard.component';
import {AlbumTableComponent} from './components/album-table.component';

//modules
import {DashboardRoutingModule} from './dashboard-routing.module';
import {SharedModule} from '../shared/shared.module';

@NgModule({
    imports: [
        CommonModule,
        DashboardRoutingModule,
        NgxPaginationModule,
        SharedModule
    ],
    declarations: [
        DashboardComponent,
        AlbumTableComponent
    ]
})
export class DashboardModule{

}