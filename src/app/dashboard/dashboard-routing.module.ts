import {NgModule} from '@angular/core';
import {Route, RouterModule} from '@angular/router';

//components
import {DashboardComponent} from './dashboard.component';

const ROUTES: Route[] = [
    {
        path: '',
        component: DashboardComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(ROUTES)
    ],
    exports: [
        RouterModule
    ]
})
export class DashboardRoutingModule{

}