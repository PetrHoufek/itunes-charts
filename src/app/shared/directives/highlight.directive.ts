import {Directive, ElementRef, HostListener, Input} from '@angular/core';

@Directive({
    selector: '[highlight]'
})
export class HighlightDirective{

    color: string = '#eaf9ff';
    @Input() isRowEven: boolean;

    constructor(
        private elementRef: ElementRef
    ){

    }

    @HostListener('mouseenter') onMouseEnter(){
        this.elementRef.nativeElement.style.backgroundColor = this.color;
    }

    @HostListener('mouseleave') onMouseLeave(){
        if(this.isRowEven){
            this.elementRef.nativeElement.style.backgroundColor = 'rgb(250, 250, 250)';
        }else{
            this.elementRef.nativeElement.style.backgroundColor = 'rgb(247, 247, 247)';
        }
    }

}