import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'filterarray'
})
export class FilterArrayPipe implements PipeTransform{

    transform(values: any[], key: any, type: string): any[]{
        if(!values){
            return;
        }
        let result = values.filter(e => {
            if(key != undefined && key != ""){
                if(type == 'album' && e['im:name'] && e['im:name'].label){
                    return (<string>e['im:name'].label).toLowerCase().includes(key.toLowerCase());
                }
                if(type == 'artist' && e['im:artist'] && e['im:artist'].label){
                    return (<string>e['im:artist'].label).toLowerCase().includes(key.toLowerCase());
                }
                if(type == 'category' && e.category && e.category.attributes && e.category.attributes.label){
                    return (<string>e.category.attributes.label).toLowerCase().includes(key.toLowerCase());
                }
            }
            return true;
        });
        return result;
    }

}