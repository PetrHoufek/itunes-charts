import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

//pipes
import {FilterArrayPipe} from './pipes/filter-array.pipe';

//directives
import {HighlightDirective} from '../shared/directives/highlight.directive';

@NgModule({
    declarations: [
        FilterArrayPipe,
        HighlightDirective
    ],
    exports: [
        FilterArrayPipe,
        FormsModule,
        HighlightDirective
    ]
})
export class SharedModule{
    
}