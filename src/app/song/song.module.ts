import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

//modules
import {SongRoutingModule} from './song-routing.module';
import {SharedModule} from '../shared/shared.module';

//components
import {SongComponent} from './song.component'

@NgModule({
    imports: [
        CommonModule,
        SongRoutingModule,
        SharedModule
    ],
    declarations: [
        SongComponent
    ]
})
export class SongModule{

}