import {NgModule} from '@angular/core';
import {Route, RouterModule} from '@angular/router'

//components
import {SongComponent} from './song.component';

const ROUTES: Route[] = [
    {
        path: 'song/:id',
        component: SongComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(ROUTES)
    ],
    exports: [
        RouterModule
    ]
})
export class SongRoutingModule{

}