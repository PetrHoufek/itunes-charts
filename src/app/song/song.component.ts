import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params} from '@angular/router';

//services
import {Entry, Feed, HttpService} from '../core/services/http.service';

@Component({
    templateUrl: './song.component.html',
    styleUrls: ['./song.component.scss']
})
export class SongComponent implements OnInit{
    
    entry: Entry;

    constructor(
        private activatedRoute: ActivatedRoute,
        private httpService: HttpService
    ){

    }

    ngOnInit(){
        let params: Params = this.activatedRoute.params;
        if(params){
            this.httpService.getFeed().subscribe((data: Feed) => {
                this.entry = data.feed.entry.find(e => {
                    if(e.id && e.id.attributes && e.id.attributes){
                        return +params.value['id'] == e.id.attributes['im:id'];
                    }
                    return false;
                })
                console.log('entry: ', this.entry);
            });
        }
    }

}