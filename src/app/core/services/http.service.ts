import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs';

/**
 * I was looking at https://www.apple.com/itunes/charts/albums/ and into the list
 * of objects i received from ITUNES_URL but there seems to be no property based
 * on which i could sort these songs, i guess i have to rely on the order in which
 * they are in the json file.
 */
const ITUNES_URL = "https://itunes.apple.com/us/rss/topalbums/limit=100/json";

export interface Feed{
    feed: {
        author: any;
        entry: any[];
        missingproperty: any;
    }
}

export interface Entry{
    
}

@Injectable()
export class HttpService{

    feed: Feed;
    observable: Observable<Feed>;

    constructor(
        private httpClient: HttpClient
    ){

    }

    getFeed(): Observable<Feed>{
        if(this.feed){
            return of(this.feed);
        }else if(this.observable){
            return this.observable;
        }else{
            this.observable = this.httpClient.get<Feed>(ITUNES_URL);
            this.observable.subscribe((data: Feed) => this.feed = { ...data });
            return this.observable;
        }
    }

}