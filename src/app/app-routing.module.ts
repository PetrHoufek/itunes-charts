import {NgModule} from '@angular/core';
import {Route, RouterModule} from '@angular/router';

const ROUTES: Route[] = [
]

@NgModule({
    imports: [
        RouterModule.forRoot(ROUTES)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule{

}