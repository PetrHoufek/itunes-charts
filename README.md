1 Clone the repository using "git clone https://bitbucket.org/PetrHoufek/itunes-charts/src/master/"  
2 Execute "npm install"  
3 Execute "ng serve --open"  

![Scheme](https://bitbucket.org/PetrHoufek/itunes-charts/downloads/intro1.jpg)
![Scheme](https://bitbucket.org/PetrHoufek/itunes-charts/downloads/intro2.jpg)